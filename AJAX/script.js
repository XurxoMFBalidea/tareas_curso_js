const $ = (id) => document.getElementById(id);

const $xmlhr = $("xmlhttprequest"),
    $fetch = $("fetch"),
    $axios = $("axios"),
    $btnAnt = $("ant"),
    $btnSig = $("sig"),
    METHOD = "GET",
    URL = "https://jsonplaceholder.typicode.com/albums",
    CANT_REG = 10;

let actPage = 0;
let totalPages;

// XMLHttpRequest
const xmfhtrFunc = () => {
    const req = new XMLHttpRequest(),
        frag = document.createDocumentFragment();

    req.addEventListener("readystatechange", (e) => {
        if (req.readyState !== 4) return;

        if (req.status >= 200 && req.status <= 299) {
            let jsonReq = JSON.parse(req.responseText);

            //Isto é para saber a cantidade de páxinas que teríamos, para a paxinación. Solo o fago en XMLHttpRequest pq todos os métodos van retornar o mesmo, é a mesma consulta.
            totalPages = (jsonReq.length / CANT_REG) % CANT_REG == 0 ? Math.floor(jsonReq.length / CANT_REG) - 1 : Math.floor(jsonReq.length / CANT_REG) - 2;

            for (let i = actPage * CANT_REG; i < (actPage + 1) * CANT_REG && i < jsonReq.length; i++) {
                const $res = document.createElement("p");
                $res.innerHTML = `ID: ${jsonReq[i].id}, Título: ${jsonReq[i].title}, Usuario: ${jsonReq[i].userId}`;
                frag.appendChild($res);
            }

            $xmlhr.appendChild(frag);
        } else {
            console.error(req.status, req.statusText);
        }
    });

    req.open(METHOD, URL);
    req.send();
};

// Fetch
const fetchFunc = () => {
    const frag = document.createDocumentFragment();

    (async () => {
        let res = await fetch(URL),
            json = await res.json();

        try {
            if (!res.ok) throw `${res.status}, ${res.statusText}`;

            for (let i = actPage * CANT_REG; i < (actPage + 1) * CANT_REG && i < json.length; i++) {
                const $res = document.createElement("p");
                $res.innerHTML = `ID: ${json[i].id}, Título: ${json[i].title}, Usuario: ${json[i].userId}`;
                frag.appendChild($res);
            }

            $fetch.appendChild(frag);
        } catch (err) {
            console.log(err);
        }
    })();
};

//axios
const axiosFunc = () => {
    const frag = document.createDocumentFragment();

    (async () => {
        try {
            let res = await axios.get("https://jsonplaceholder.typicode.com/albums"),
                json = res.data;

            if (res.request.status >= 200 && res.request.status <= 299) {
                for (let i = actPage * CANT_REG; i < (actPage + 1) * CANT_REG && i < json.length; i++) {
                    const $res = document.createElement("p");
                    $res.innerHTML = `ID: ${json[i].id}, Título: ${json[i].title}, Usuario: ${json[i].userId}`;
                    frag.appendChild($res);
                }

                $axios.appendChild(frag);
            }
        } catch (error) {
            console.error(error.request.status, error.request.statusText);
        }
    })();
};

const getAll = () => {
    let reload = document.querySelectorAll(".reload");
    reload.forEach((e) => {
        e.innerHTML = "";
    });
    xmfhtrFunc();
    fetchFunc();
    axiosFunc();
};
getAll();

$btnAnt.addEventListener("click", () => {
    if (actPage <= 0) return;
    if (actPage == totalPages) $btnSig.disabled = false;
    if (actPage == 1) $btnAnt.disabled = true;
    actPage--;
    getAll();
});

$btnSig.addEventListener("click", () => {
    console.log(totalPages);
    if (actPage >= totalPages) return;
    if (actPage == 0) $btnAnt.disabled = false;
    if (actPage == totalPages - 1) $btnSig.disabled = true;
    actPage++;
    getAll();
});
